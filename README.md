This program finds element in original XML file having id=make-everything-ok-button.
Than it finds element supposed to have the same purpose in another (diff) xml file.

Output is performed to console.
Expected output:
1) How many elements matching original one have been found in diff xml file
2) Attributes of matching XML elements found
3) Path to matching elements

Output example:

[INFO] 2018-05-11 08:19:56,020 c.a.Main - Found 1 matching elements
[INFO] 2018-05-11 08:19:56,024 c.a.Main -  class="btn test-link-ok" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okComplete(); return false;"
[INFO] 2018-05-11 08:19:56,027 c.a.Main - html > body > div > div > div > div > div > div > div > a


Usage:

cd artifact;
java -jar xml_analyzer.jar [path to xml file with original element] [path to diff xml file]