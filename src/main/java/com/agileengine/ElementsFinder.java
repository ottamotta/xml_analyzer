package com.agileengine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ElementsFinder {

    private static Logger LOGGER = LoggerFactory.getLogger(ElementsFinder.class);

    private static String CHARSET_NAME = "utf8";

    public List<Element> findAll(File htmlFile) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());
            return doc.select("*");
        } catch (IOException e ) {
            throw new RuntimeException(e);
        }
    }

    public Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.of(doc.getElementById(targetElementId));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPath(Element element) {
        Deque<String> segments = new ArrayDeque<>();
        while (element.hasParent()) {
            segments.push(element.tagName());
            element = element.parent();
        }
        return segments.stream().collect(Collectors.joining(" > "));
    }


}