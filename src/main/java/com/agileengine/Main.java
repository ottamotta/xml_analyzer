package com.agileengine;

import javafx.util.Pair;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

public class Main {

    private static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        if (args.length != 2) {
            LOGGER.error("This application requires exactly 2 arguments: path to original (1) and path to input (2) XML documents, but " + args.length + " args provided");
            System.exit(1);
        }

        String originalPath = args[0];
        String diffPath = args[1];

        ElementsFinder elementsFinder = new ElementsFinder();
        elementsFinder.findElementById(new File(originalPath), "make-everything-ok-button").ifPresent(
                original -> {

                    List<Element> elements = elementsFinder.findAll(new File(diffPath));
                    Set<Element> matches = elementsWithTopMatches(original, elements);

                    if (matches.size() > 0) {
                        LOGGER.info("Found " + matches.size() + " matching elements");
                        matches.forEach(element -> {
                            LOGGER.info(element.attributes().toString());
                            LOGGER.info(elementsFinder.getPath(element));
                        });

                    } else {
                        LOGGER.info("No elements matching original have been found");
                    }
                }
        );

    }

    private static Set<Element> elementsWithTopMatches(Element original, List<Element> input) {

        Map<Integer, Set<Pair<Element, Integer>>> matchesCountByPairs = input.stream()
                        .map(element -> new Pair<>(element, attributeMatchesCount(original, element)))
                        .filter(pair -> pair.getValue() > 0)
                        .collect(groupingBy(Pair::getValue, toSet()));


        Optional<Integer> maxMatchesOpt = matchesCountByPairs.keySet().stream()
                .max(Comparator.naturalOrder());

        int maxMatches = maxMatchesOpt.orElseThrow(() -> new RuntimeException("No matching elements found"));

        return matchesCountByPairs.get(maxMatches).stream()
                .map(Pair::getKey)
                .collect(toSet());

    }

    private static int attributeMatchesCount(Element original, Element input) {
        int matchCounter = 0;
        for (Attribute attr : original.attributes()) {
            if (input.hasAttr(attr.getKey()) && input.attr(attr.getKey()).equalsIgnoreCase(original.attr(attr.getKey()))) {
                matchCounter++;
            }
        }
        return matchCounter;
    }
}
